FROM centos:7

# Basic system installation
RUN yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm
RUN yum -y install epel-release 
RUN yum -y install python-pip  gcc HEP_OSlibs python-devel && yum clean all

# Installing the LHCb base software
RUN pip install lbinstall
RUN lbinstall --root=/opt/lhcb install LBSCRIPTSDEV CMT

